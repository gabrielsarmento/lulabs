from api.favorite.Service import FavoriteService
from api.user.Service import UserService
from database.Mongo import db
from pymongo import ASCENDING
from database.Resources import *


class Seed:
    def __init__(self):
        self.db = db
        self.users = db.get_collection('users')
        self.customers = db.get_collection('customers')
        self.favorites = db.get_collection('favorites')
        self.user_service = UserService()
        self.favirite_service = FavoriteService()

    def drop_databases(self):
        self.users.drop()
        self.customers.drop()
        self.favorites.drop()

    def create_indexes(self):
        self.users.create_index([('email', ASCENDING)], unique=True)
        self.customers.create_index([('email', ASCENDING)], unique=True)
        self.favorites.create_index([('customer_id', ASCENDING), ('product_id', ASCENDING)], unique=True)

    def create_admin_user(self):
        self.user_service.create_user(**user_admin)

    def create_customers(self):
        for customer in customers:
            self.customers.insert_one(customer)

    def create_favorites(self):
        for product in products:
            self.favirite_service.create_favorite({'customer_id': 'CUSTOMER_01_ID', 'product_id': product.get('id')})

    def run(self):
        self.drop_databases()
        self.create_indexes()
        self.create_admin_user()
        self.create_customers()
        self.create_favorites()

