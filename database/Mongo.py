from pymongo import MongoClient, uri_parser
from api.utils.env import envs


url = uri_parser.parse_uri(envs.MONGO_URL)

client = MongoClient(host=envs.MONGO_URL)

db = client.get_database(url['database'])
