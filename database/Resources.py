user_admin = {
    'name': 'Admin user',
    'password': '123456',
    'role': 'ADMIN',
    'email': 'admin@test.com'
}


customers = {
    '_id': 'CUSTOMER_01_ID',
    'name': 'Customer 01',
    'email': 'customer01@email.com'
}, {
    '_id': 'CUSTOMER_02_ID',
    'name': 'Customer 02',
    'email': 'customer02@email.com'
}

products = {
    'price': 1699.0,
    'image': 'http://challenge-api.luizalabs.com/images/1bf0f365-fbdd-4e21-9786-da459d78dd1f.jpg',
    'brand': 'bébé confort',
    'id': '1bf0f365-fbdd-4e21-9786-da459d78dd1f',
    'title': 'Cadeira para Auto Iseos Bébé Confort Earth Brown'
}, {
    'price': 1149.0,
    'image': 'http://challenge-api.luizalabs.com/images/958ec015-cfcf-258d-c6df-1721de0ab6ea.jpg',
    'brand': 'bébé confort',
    'id': '958ec015-cfcf-258d-c6df-1721de0ab6ea',
    'title': 'Moisés Dorel Windoo 1529'
}
