import unittest
from api import create_app
import json
from tests.Resources import *
from api.product.Errors import ProductNotFoundException
from api.favorite.Errors import *


class TestFavorite(unittest.TestCase):

    def setUp(self) -> None:
        self.app = create_app().test_client()

    def test_create_favorite(self):
        result = self.app.post('/v1/favorites', json=favorite_ok)
        self.assertEqual(201, result.status_code)

    def test_create_favorite_not_existent_product(self):
        result = self.app.post('/v1/favorites', json=favorite_error)
        self.assertEqual(ProductNotFoundException.http_error_code, result.status_code)
        self.assertEqual(ProductNotFoundException.domain_error_code, json.loads(result.data.decode()).get('code'))

    def test_favorite_a_product_twice(self):
        result = self.app.post('/v1/favorites', json=favorite_ok)
        self.assertEqual(201, result.status_code)
        result = self.app.post('/v1/favorites', json=favorite_ok)
        self.assertEqual(ProductAlreadyFavoriteException.http_error_code, result.status_code)
        self.assertEqual(ProductAlreadyFavoriteException.domain_error_code, json.loads(result.data.decode()).get('code'))
