admin_token = {'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.'
                                'eyJfaWQiOiJhMjQ4NGQ3Zi1jOTc1LTRjNWEtYTBiZC1hZTM0YmY0MWE3ZmQiLCJyb2xlIjoiQURNSU4ifQ.'
                                'Nm1xaFfSe95BtxLqOw21myfGQCCmCGXJ-aYDcrC_5Ig'}


customer_test = {
    'name': 'luisa',
    'email': 'luisa@email.com'
}

customer_01 = {
    '_id': 'CUSTOMER_01_ID',
    'name': 'Customer 01',
    'email': 'customer01@email.com'
}

custom_customer = {
    'name': 'custom_name',
    'email': 'custom@email.com'
}

favorite_error = {
    'customer_id': 'CUSTOMER_01_ID',
    'product_id': 'PRODUCT_ERROR_ID'
}

favorite_ok = {
    'customer_id': 'CUSTOMER_01_ID',
    'product_id': '571fa8cc-2ee7-5ab4-b388-06d55fd8ab2f'
}

products = [
    {
        'price': 1149.0,
        'image': 'http://challenge-api.luizalabs.com/images/958ec015-cfcf-258d-c6df-1721de0ab6ea.jpg',
        'brand': 'bébé confort',
        'id': '958ec015-cfcf-258d-c6df-1721de0ab6ea',
        'title': 'Moisés Dorel Windoo 1529'
    },
    {
        'price': 1699.0,
        'image': 'http://challenge-api.luizalabs.com/images/1bf0f365-fbdd-4e21-9786-da459d78dd1f.jpg',
        'brand': 'bébé confort',
        'id': '1bf0f365-fbdd-4e21-9786-da459d78dd1f',
        'title': 'Cadeira para Auto Iseos Bébé Confort Earth Brown'
    }
]
