import unittest
from api import create_app
import json
from tests.Resources import *
from api.customer.Errors import *


class TestCustomer(unittest.TestCase):
    def setUp(self) -> None:
        self.app = create_app().test_client()

    def test_create_customer(self):
        result = self.app.post('/v1/customers', json=customer_test, headers=admin_token)
        self.assertEqual(201, result.status_code)

    def test_create_customer_same_email(self):
        result = self.app.post('/v1/customers', json=customer_test, headers=admin_token)
        self.assertEqual(201, result.status_code)

        result = self.app.post('/v1/customers', json=customer_test, headers=admin_token)
        self.assertEqual(CustomerEmailAlreadyTakenException.http_error_code, result.status_code)
        self.assertEqual(CustomerEmailAlreadyTakenException.domain_error_code, json.loads(result.data.decode()).get('code'))

    def test_get_customer(self):
        result = self.app.get('/v1/customers/CUSTOMER_01_ID', headers=admin_token)
        self.assertEqual(200, result.status_code)
        self.assertEqual(dict(**customer_01, favorite_products=products),
                         json.loads(result.data.decode()))

    def test_get_not_exists_customer(self):
        result = self.app.get('/v1/customers/CUSTOMER_ERROR_ID', headers=admin_token)
        self.assertEqual(CustomerNotFoundException.http_error_code, result.status_code)
        self.assertEqual(CustomerNotFoundException.domain_error_code, json.loads(result.data.decode()).get('code'))

    def test_delete_customer(self):
        result = self.app.post('/v1/customers', json=customer_test, headers=admin_token)
        self.assertEqual(201, result.status_code)
        customer_id = json.loads(result.data.decode()).get('_id')
        result = self.app.delete('/v1/customers/'+customer_id, headers=admin_token)
        self.assertEqual(200, result.status_code)

    def test_delete_not_exists_customer(self):
        result = self.app.delete('/v1/customers/CUSTOMER_ERROR_ID', headers=admin_token)
        self.assertEqual(CustomerNotFoundException.http_error_code, result.status_code)
        self.assertEqual(CustomerNotFoundException.domain_error_code, json.loads(result.data.decode()).get('code'))

    def test_update_a_customer_by_id(self):
        result = self.app.post('/v1/customers', json=customer_test, headers=admin_token)
        self.assertEqual(201, result.status_code)
        customer_id = json.loads(result.data.decode()).get('_id')
        result = self.app.put('/v1/customers/'+customer_id, json=custom_customer, headers=admin_token)
        self.assertEqual(200, result.status_code)
        self.assertEqual(dict(**custom_customer, _id=customer_id), json.loads(result.data.decode()))
