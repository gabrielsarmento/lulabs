-i https://pypi.org/simple
aniso8601==8.0.0
attrs==19.2.0
bcrypt==3.1.7
certifi==2019.9.11
cffi==1.12.3
chardet==3.0.4
click==7.0
flask-cors==3.0.8
flask-marshmallow==0.10.1
flask-restplus==0.13.0
flask==1.1.1
gunicorn==19.9.0
idna==2.8
itsdangerous==1.1.0
jinja2==2.10.2
jsonschema==3.0.2
markupsafe==1.1.1
marshmallow==3.2.1
pycparser==2.19
pyjwt==1.7.1
pymongo==3.9.0
pyrsistent==0.15.4
pytz==2019.2
requests==2.22.0
six==1.12.0
urllib3==1.25.6
werkzeug==0.16.0
