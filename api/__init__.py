from flask import Flask
from flask_restplus import Api
from database.Seed import Seed
from api.config import Cors, Namespaces, JsonEncoder
from api.utils.env import envs
from api import errors


def create_app():
    app = Flask(__name__)
    app.json_encoder = JsonEncoder.CustomJSONEncoder
    Cors.configure(app)
    api = Api(
        app,
        title=' Scaffold Flask API',
        version='0.1',
        doc='/' if envs.is_development else False
    )
    app.config.update(ERROR_404_HELP=False)
    Namespaces.configure(api)
    errors.configure(api)
    if envs.is_development:
        Seed().run()

    return app
