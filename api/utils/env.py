import os
from dataclasses import dataclass


@dataclass
class ENVS:
	ENV = os.getenv('ENV')
	MONGO_URL = os.getenv('MONGO_URL')
	KEY = os.getenv('KEY')
	PRODUCT_API_URL = os.getenv('PRODUCTS_API_URL')

	@property
	def is_development(self):
		return self.ENV == 'development'


envs = ENVS()
