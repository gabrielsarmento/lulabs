import functools
from flask import request
from flask_restplus import abort
import jwt
import datetime
import os
from api.user.Model import User


def gen_jwt(user: User) -> str:
    payload = {
        '_id': user.id,
        'role': user.role,
        # 'exp': datetime.datetime.utcnow() + datetime.timedelta(days=3)
    }

    token = jwt.encode(payload=payload, key=os.environ['KEY'])
    return token.decode()


def get_bearer(user: User):
    return {'Authorization': f'Bearer {gen_jwt(user)}'}


def jwt_required(roles: list = False, user_id=False):
    def login_required(method):
        @functools.wraps(method)
        def wrapper(*args, **kwargs):
            header = request.headers.get('Authorization')
            if not header:
                abort(403, message='No token here')
            token = header.split()[1]
            try:
                decoded = jwt.decode(token, os.environ['KEY'], algorithms='HS256')
                if user_id:
                    kwargs['user_id'] = decoded['_id']

                if roles and not decoded['role'] in roles:
                    abort(403, message='Not a valid role.')
                return method(*args, **kwargs)
            except jwt.DecodeError:
                abort(403, message='Token is not valid.')
            except jwt.ExpiredSignatureError:
                abort(403, message='Token is expired.')

        return wrapper
    return login_required
