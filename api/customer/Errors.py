from api.errors.BaseDomainError import BaseDomainException


class CustomerNotFoundException(BaseDomainException):
    http_error_code = 404
    domain_error_code = 300
    message = 'Customer Not Found'


class CustomerEmailAlreadyTakenException(BaseDomainException):
    http_error_code = 400
    domain_error_code = 301
    message = 'Email Already Taken'
