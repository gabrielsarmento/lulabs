from flask_restplus import Namespace, Resource, fields
from flask import jsonify, make_response
from api.customer.Service import CustomerService
from api.utils.jwt import jwt_required


customers = Namespace('Customers', path='/v1/customers')

customer_model = customers.model('Customer', {
    'name': fields.String(required=True),
    'email': fields.String(required=True)
})

customer_service = CustomerService()


@customers.route('')
class Customers(Resource):

    @jwt_required(['ADMIN'])
    @customers.expect(customer_model, validate=True)
    def post(self):
        customer = customer_service.create_customer(customers.payload)
        return make_response(jsonify(customer), 201)


@customers.route('/<customer_id>')
class CustomersCustomer(Resource):
    @jwt_required(['ADMIN'])
    def get(self, customer_id):
        customer = customer_service.get_customer_with_favorite_products(customer_id)
        return make_response(jsonify(customer), 200)

    @jwt_required(['ADMIN'])
    def delete(self, customer_id):
        customer = customer_service.delete_customer_by_id(customer_id)
        return make_response(jsonify(customer), 200)

    @jwt_required(['ADMIN'])
    @customers.expect(customer_model, validate=True)
    def put(self, customer_id):
        customer = customer_service.update_customer(customer_id=customer_id, content=customers.payload)
        return make_response(jsonify(customer), 200)
