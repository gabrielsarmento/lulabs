from dataclasses import dataclass


@dataclass
class CustomerDTO:
    _id: str
    name: str
    email: str
    favorite_products: list
