from dataclasses import dataclass, asdict


@dataclass
class Customer:
    _id: str
    name: str
    email: str

    @property
    def id(self):
        return self._id

    def dict(self):
        return asdict(self)
