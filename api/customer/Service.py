from api.customer.Repository import CustomerRepository
from api.customer.Model import Customer
from api.customer.Errors import CustomerNotFoundException
from api.customer.dto.CustomerDTO import CustomerDTO
from api.favorite.Service import FavoriteService
from uuid import uuid4


class CustomerService:
    def __init__(self):
        self.customer_repository = CustomerRepository()
        self.favorite_service = FavoriteService()

    def create_customer(self, customer) -> Customer:
        customer = Customer(**customer, _id=str(uuid4()))
        self.customer_repository.save_customer(customer)
        return customer

    def get_customer_by_id(self, customer_id) -> Customer:
        customer = self.customer_repository.find_customer_by_id(customer_id)
        if not customer:
            raise CustomerNotFoundException
        return customer

    def delete_customer_by_id(self, customer_id) -> Customer:
        customer = self.get_customer_by_id(customer_id)
        self.customer_repository.delete_customer(customer)
        return customer

    def update_customer(self, customer_id, content) -> Customer:
        customer = self.get_customer_by_id(customer_id)
        customer.name = content.get('name')
        customer.email = content.get('email')
        self.customer_repository.replace_customer(customer)
        return customer

    def get_customer_with_favorite_products(self, customer_id) -> CustomerDTO:
        customer = self.get_customer_by_id(customer_id)
        favorite_products = self.favorite_service.get_favorites_from_customer_by_customer_id(customer_id)
        return CustomerDTO(**customer.dict(), favorite_products=favorite_products)
