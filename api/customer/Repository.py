from database.Mongo import db
from api.customer.Model import Customer
from dataclasses import asdict
from pymongo.errors import DuplicateKeyError
from api.customer.Errors import CustomerEmailAlreadyTakenException


class CustomerRepository:
    def __init__(self):
        self.customers = db.get_collection('customers')

    def save_customer(self, customer: Customer):
        try:
            self.customers.insert_one(asdict(customer))
        except DuplicateKeyError:
            raise CustomerEmailAlreadyTakenException(customer.email)

    def find_customer_by_id(self, customer_id: str):
        result = self.customers.find_one({'_id': customer_id})
        return Customer(**result) if result else result

    def replace_customer(self, customer: Customer):
        self.customers.find_one_and_replace({'_id': customer.id}, asdict(customer))

    def delete_customer(self, customer: Customer):
        self.customers.find_one_and_delete({'_id': customer.id})
