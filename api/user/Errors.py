from api.errors.BaseDomainError import BaseDomainException


class UserNotFoundException(BaseDomainException):
    http_error_code = 404
    domain_error_code = 100
    message = 'User Not Found'
