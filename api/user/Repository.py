from database.Mongo import db
from api.user.Model import User
from dataclasses import asdict
import uuid


class UserRepository:
    def __init__(self):
        self.users = db.get_collection('users')

    def save_user(self, user: User) -> User:
        user._id = str(uuid.uuid4())
        self.users.insert_one(asdict(user))
        return user

    def find_user_by_email(self, email: str) -> User:
        user = self.users.find_one({'email': email})
        return User(**user) if user else None

    def replace_user(self, user: User):
        self.users.find_one_and_replace({'_id': user.id}, asdict(user))

    def find_user_by_id(self, user_id) -> User:
        user = self.users.find_one({'_id': user_id})
        return User(**user) if user else None
