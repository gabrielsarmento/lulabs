from api.user.Repository import UserRepository
from api.user.Model import User
from api.user.Errors import *
from api.utils.password import hash_password


class UserService:
    def __init__(self):
        self.user_repository = UserRepository()

    def create_user(self, password: str, **fields) -> User:
        user = User(password=hash_password(password), **fields)
        user = self.user_repository.save_user(user)
        return user

    def update_user(self, user_id: str, password, **fields) -> User:
        user = User(_id=user_id, password=hash_password(password), **fields)
        self.user_repository.replace_user(user)
        return user

    def get_user_by_email(self, email: str) -> User:
        user = self.user_repository.find_user_by_email(email)
        return user

    def get_user_by_id(self, user_id) -> User:
        user = self.user_repository.find_user_by_id(user_id)
        if not user:
            raise UserNotFoundException
        return user
