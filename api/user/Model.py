from dataclasses import dataclass


@dataclass
class User:
    role: str
    name: str
    password: str
    email: str
    _id: str = None

    @property
    def id(self):
        return self._id
