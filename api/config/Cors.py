from flask_cors import CORS


def configure(app):
    CORS(app,
         expose_headers=['Authorization'],
         allow_headers=[
             "Content-Type",
             "Accept",
             "Authorization",
             "Access-Control_Allow-Origin",
         ],
         supports_credentials=True)
