# from api.user.Controller import users
from api.login.Controller import login
from api.customer.Controller import customers
from api.favorite.Controller import favorites


def configure(api):
    # api.add_namespace(users)
    api.add_namespace(login)
    api.add_namespace(customers)
    api.add_namespace(favorites)
