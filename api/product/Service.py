from api.product.Errors import *
from api.utils.env import envs
import json
import requests


class ProductService:
    @staticmethod
    def verify_product_id(product_id):
        result = requests.get(envs.PRODUCT_API_URL+product_id)
        if result.status_code == 404:
            raise ProductNotFoundException
        return json.loads(result.content.decode())
