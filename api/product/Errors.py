from api.errors.BaseDomainError import BaseDomainException


class ProductNotFoundException(BaseDomainException):
    http_error_code = 404
    domain_error_code = 200
    message = 'Product Not Found'
