from api.errors.BaseDomainError import BaseDomainException


class ProductAlreadyFavoriteException(BaseDomainException):
    domain_error_code = 502
    http_error_code = 400
    message = 'This product was already set as favorite by this customer'
