from dataclasses import dataclass


@dataclass
class Favorite:
    _id: str
    customer_id: str
    product_id: str

    @property
    def id(self):
        return self._id
