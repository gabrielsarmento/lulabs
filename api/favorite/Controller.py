from flask_restplus import Resource, fields, Namespace
from flask import jsonify, make_response
from api.favorite.Service import FavoriteService


favorites = Namespace('Favorites', path='/v1/favorites')

favorite_model = favorites.model('Favorite', {
    'customer_id': fields.String(required=True),
    'product_id': fields.String(required=True)
})


favorite_service = FavoriteService()


@favorites.route('')
class Favorites(Resource):
    @favorites.expect(favorite_model, validate=True)
    def post(self):
        favorite = favorite_service.create_favorite(favorites.payload)
        return make_response(jsonify(favorite), 201)
