from api.favorite.Repository import FavoriteRepository
from api.favorite.Model import Favorite
from uuid import uuid4
from api.product.Service import ProductService


class FavoriteService:
    def __init__(self):
        self.favorite_repository = FavoriteRepository()
        self.product_service = ProductService()

    def create_favorite(self, favorite) -> Favorite:
        favorite = Favorite(**favorite, _id=str(uuid4()))
        self.product_service.verify_product_id(favorite.product_id)
        self.favorite_repository.save_favorite(favorite)
        return favorite

    def get_favorites_from_customer_by_customer_id(self, customer_id):
        products_id = self.favorite_repository.list_favorites_products_by_customer_id(customer_id)
        products = [self.product_service.verify_product_id(product_id.get('_id')) for product_id in products_id]
        return products
