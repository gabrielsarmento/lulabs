from database.Mongo import db
from api.favorite.Model import Favorite
from dataclasses import asdict
from pymongo.errors import DuplicateKeyError
from api.favorite.Errors import ProductAlreadyFavoriteException


class FavoriteRepository:
    def __init__(self):
        self.favorites = db.get_collection('favorites')

    def save_favorite(self, favorite: Favorite):
        try:
            self.favorites.insert_one(asdict(favorite))
        except DuplicateKeyError:
            raise ProductAlreadyFavoriteException

    def list_favorites_products_by_customer_id(self, customer_id):
        pipeline = [
            {'$match': {'customer_id': customer_id}},
            {'$group': {'_id': '$product_id'}}
        ]
        return list(self.favorites.aggregate(pipeline))
