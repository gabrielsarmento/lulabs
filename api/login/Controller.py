from flask_restplus import Namespace, Resource, reqparse
import base64
import bcrypt
from api.user.Service import UserService
from api.utils.jwt import get_bearer
import jwt
from api.utils.env import envs
from flask import make_response, jsonify


login = Namespace('Login', path='/v1/login')


@login.route('')
class Login(Resource):
    user_service = UserService()

    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('Authorization', location='headers', required=True)
        auth = parser.parse_args()['Authorization']
        auth, token = auth.split()
        if auth == 'Basic':

            auth = base64.b64decode(token).decode()
            email, password = auth.split(':')

            user = self.user_service.get_user_by_email(email)
            if not user:
                login.abort(404)
            if not bcrypt.checkpw(password.encode(), user.password.encode()):
                login.abort(401)
            return make_response(jsonify(user), 200, get_bearer(user))

        elif auth == 'Bearer':
            try:
                decoded = jwt.decode(token, envs.KEY, algorithms='HS256')
                user_service = UserService()
                user = user_service.get_user_by_id(decoded['_id'])
                if user is None:
                    login.abort(404, message='user not found')
                return user.json(), 200, get_bearer(user)

            except jwt.DecodeError:
                login.abort(403, message='Token is not valid.')
            except jwt.ExpiredSignatureError:
                login.abort(403, message='Token is expired.')

        else:
            login.abort(403, message='Authentication method not valid')
