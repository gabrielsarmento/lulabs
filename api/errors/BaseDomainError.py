from werkzeug.exceptions import HTTPException


class BaseDomainException(HTTPException):
    http_error_code = 400
    message = 'Ops! Something is going wrong, please try again or contact our support Team. :)'
    domain_error_code = 0
