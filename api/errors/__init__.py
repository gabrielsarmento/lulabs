from flask_restplus import Api
from api.errors.BaseDomainError import BaseDomainException


def configure(api: Api):

    @api.errorhandler(BaseDomainException)
    def main_error_handler(error):
        return {
            'message': error.message,
            'code': error.domain_error_code,
            'meta': error.description
        }, error.http_error_code
