### Desafio técnico LuizaLabs

#### Requisitos para rodar o projeto:
* python 3.7+
* [Docker](https://gabrielsarmento.github.io/linux/ubuntu/services/docker/compose/2019/07/03/install-docker-ce-on-ubuntu-18-04.html)
* [Docker-compose](https://gabrielsarmento.github.io/linux/ubuntu/services/docker/compose/2019/07/03/install-docker-compose-on-ubuntu-18-04.html)
* Pipenv (`pip3 install --user pipenv`)

#### Como iniciar suite de testes do projeto:
```shell
pipenv install
docker-compose up mongo
pipenv run tests
```

#### Como iniciar a api:
```shell
pipenv install
docker-compose up mongo
pipenv run api
```

#### Usando docker:
```shell
docker-compose up
```


#### Documentação da api:
[WIKI](https://gitlab.com/gabrielsarmento/lulabs/wikis/home)
